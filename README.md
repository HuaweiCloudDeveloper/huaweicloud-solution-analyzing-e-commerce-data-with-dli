[TOC]

**解决方案介绍**
===============
当前线上购物无疑是最火热的购物方式，而电商平台则又可以以多种方式接入，例如通过web方式访问、通过app的方式访问、通过微信小程序的方式访问等等。该解决方案可以帮助而电商平台统计各平台的实时访问数据量、订单数、访问人数等等指标，从而能在显示大屏上实时展示相关数据。电商平台可以通过将每个商品的订单信息实时写入Kafka中或将业务数据表上传至对象存储 OBS桶中从而触发函数工作流自动将数据写入Kafka中，DLI根据当前可以获取到的业务数据，实时统计每种渠道的相关指标，输出存储到数据库中。方便电商平台及时了解数据变化，有针对性地调整营销策略。适用于通过web方式访问、通过app的方式访问、通过微信小程序的方式访问等等电商线上购物。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/analyzing-e-commerce-data-with-dli.html

**架构图**
---------------
![方案架构](./document/analyzing-e-commerce-data-with-dli.png)

**架构描述**
---------------
该解决方案会部署如下资源：
1. 创建一台云数据库RDS for MySQL实例，用于存储统计的订单指标数据，从而展示在数据可视化DLV大屏。
2. 创建分布式消息服务 kafka专享版实例，并创建 Topic，用于电商平台写入业务数据，为用户提供便捷高效的消息队列。
3. 创建数据湖探索DLI增强型跨源队列，通过创建Flink作业，用于分析和处理电商平台的业务数据。创建一个对象存储服务OBS桶，用于用户上传业务数据表csv文件。
4. 使用函数工作流 FunctionGraph创建一个函数，通过OBS触发器，实现将用户上传至OBS桶中的业务数据写入Kafka，同时在RDS创建数据结果表模板。
<br>
用户可以购买并配置数据可视化 DLV大屏，快速定制数据大屏，将RDS中分析结果数据展示在大屏中。

**组织结构**
---------------

``` lua
huaweicloud-solution-analyzing-e-commerce-data-with-dli
├── analyzing-e-commerce-data-with-dli.tf.json -- 资源编排模板
├── functiongraph
    ├── write_data_to_kafka_and_mysql.py  -- 函数文件
```

**开始使用**
---------------
1、查看OBS桶。在控制台单击“服务列表”，选择“对象存储服务 OBS”，单击进入OBS页面。该桶将作为用户上传电商数据文件的存储桶。

图1 OBS桶

![OBS桶](./document/readme-image-001.png)

2、查看Kafka。

1. 在控制台单击“服务列表”，选择“分布式消息服务Kafka版”，在“Kafka专享版”页面找到该方案所创建的Kafka实例。

	图2 Kafka实例
	![Kafka实例](./document/readme-image-002.png)

2. 创建Kafka消费组。单击“消费组管理>创建消费组”，输入消费组名字“trade_order”(使用demo样例时请填写该名称)，单击“确定”保存。

	图3 创建消费组
	![创建消费组](./document/readme-image-003.png)

3、查看RDS。在控制台单击“服务列表”，选择“云数据库RDS”，单击进入RDS页面。在“实例管理页面”，找到该解决方案已经创建的RDS实例。

图4 RDS for MySQL实例
![RDS for MySQL实例](./document/readme-image-004.png)

4、查看DLI。在控制台单击“服务列表”，选择“数据湖探索 DLI”，单击进入DLI服务页面。单击“资源管理 > 队列管理”，查询创建的DLI队列。

图4 DLI队列
![DLI队列](./document/readme-image-005.png)

5、配置DLI服务授权。单击“全局变量>服务授权”，选定以下两项委托授权，单击“更新委托权限”。

图4 DLI服务授权
![DLI服务授权](./document/readme-image-006.png)

6、参考数据湖探索 DLI使用指南创建Flink作业，启动成功后作业状态为“运行中”。

7、体验Demo样例。获取[Demo数据样例文件](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/analyzing-e-commerce-data-with-dli/functiongraph/data.csv)，上传至步骤1中的OBS桶中，即可触发函数工作流去主动将数据写入Kafka，并自动在MySQL中创建结果表。手动创建请参考云数据库RDS for MySQL使用指南创建结果存储表，待Flink作业创建成功后参考分布式消息服务 Kafka使用指南连接Kafka手动写入数据（或参考函数工作流FunctionGraph定制化开发修改代码并重新部署，上传业务数据csv文件至OBS桶即可自动写入数据）。

8、进入Flink作业，单击“任务列表”，查看任务状态。

图4 任务列表
![任务列表](./document/readme-image-007.png)

8、登录数据库，“SQL操作”>“SQL查询”，执行如下SQL语句，即可查询到经过Flink作业处理后的结果数据。

图4 结果表
![结果表](./document/readme-image-008.png)

9、如需在可视化数据 DLV大屏展示数据，请参考配置数据可视化 DLV大屏进行配置。配置效果如下：

图4 DLV大屏展示
![DLV大屏展示](./document/readme-image-009.png)

